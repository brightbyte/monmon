const charts = [
	{ name: 'mwmonsters', title: 'Monster Classes', colors: 'data/bad-colors.csv' },
	{ name: 'mwvalues', data: 'data/mwmonsters.data', title: 'Value Classes', colors: 'data/good-colors.csv' },
	{ name: 'mwdowncasts', title: 'Downcasts', colors: 'data/cast-colors.csv' },
	{ name: 'mwdeprecations', title: 'Deprecations', colors: 'data/deprecation-colors.csv' },
];

loadCharts( charts );

function loadCharts( charts ) {
	charts.forEach( function( { name, title } ) {
		addCanvas(name);
	} );

	charts.forEach( function( chart ) {
		const url = chart.colors;
		chart.colors = {};

		fetch(url)
			.then(function (response) {
				if ( response.status == 200 ) {
					console.log(url, response.status);
					return response.text();
				} else {
					return null;  
				}
			})
			.then(function (text) {
				const colorData = CSV.parse(text, { delimiter: "\t" });
				
				colorData.forEach( function( row ) {
					const name = row[0];
					chart.colors[name] = row[1];
				} );
				// console.log( chart.colors );
				loadChartData( chart );
			})
			.catch(function (error) {
				//Something went wrong
				console.log(url, error);
				loadChartData( chart );
			});
	} );
}

function loadChartData( { name, title, colors, data } ) {
	const url = data ? data : "data/"+name+".data";
	fetch(url)
	   .then(function (response) {
		  if ( response.status == 200 ) {
			console.log(url, response.status);
			return response.text();
		  } else {
			return null;  
		  }
	   })
	   .then(function (text) {
		   if ( text ) {
				let series = csvToSeries(text, colors);
				renderChart(series, name+"-canvas", title);
		   } else {
				showError( 'Faield to load data from '+url, name+'-chart' );
		   }
	   })
	   .catch(function (error) {
		  //Something went wrong
		  console.log(url, error);
	   });
}

function addCanvas(name) {
	const canvas = document.createElement('canvas');
	canvas.id = name+'-canvas';
	
	const div = document.createElement('div');
	div.id = name+'-chart';
	div.className = 'chart';
	div.appendChild(canvas);
	
	const container = document.getElementById('chart-container');
	container.appendChild(div);
}

function showError(text, divId) {
	const div = document.createElement('div');
	div.className = 'error';
	div.appendChild( document.createTextNode(text) );

	const container = document.getElementById(divId);
	container.appendChild(div);
}

function renderChart(series, canvas, title) {
	// console.log(series);
	var ctx = document.getElementById(canvas).getContext('2d');
	
	var chart = new Chart(ctx, {
		label: "Foobar",
		type: 'line',
		data: series,
		options: {
			title: {
				display: true,
				text: title
			},
			fill: false,
			tooltips: {
				mode: "index",
				intersect: false
			},
			hover: {
				mode: "nearest",
				intersect: true
			},
			scales: {
			  xAxes: [
				{
				  type: "time",
				  time: {
						parser: 'YYYY-MM-DD',
						tooltipFormat: 'll'
				  },
				  scaleLabel: {
						display: true,
				  },
				  display: true,
				}
			  ],
			  yAxes: [
				{
				  display: true,
				  ticks: {
					  beginAtZero: true
				  },
				}
			  ]
			}
		},
	
    } );
	return chart;
}

function csvToSeries(text, colors) {
	let data = CSV.parse(text, { delimiter: "\t" });
	
	let series = [];
	let labels = [];
	const columns = data[0];

	for ( let i=1; i<columns.length; i++ ) {
		const col = columns[i];
		
		if ( !col ) {
			continue;
		}
		
		if ( !colors[ col ] ) {
			if ( colors[ '*' ] ) {
				colors[ col ] = colors[ '*' ];
			} else {
				console.log( 'no color for ' + col )
				colors[ col ] = null;
				continue;
			}
		}
		
		if ( colors[ col ] === '-' || colors[ col ] === 'hide' ) {
			colors[ col ] = null;
			continue;
		}
		
		series[col] = {
			fill: false,
			label: col,
			borderColor: colors[ col ],
			data: [],
		};
	}

	for ( let r=1; r<data.length; r++) {
		t = data[r][0];
		labels.push( t );
		
		for ( let i=1; i<data[r].length; i++ ) {
			let col = columns[i];

			if ( !data[r][i] ) {
				continue;
			}

		
			if ( !colors[ col ] ) {
				continue;
			}

			if ( !col ) {
				col = "Unknown " + i;
				columns[i] = col,
				series[col] = {
					fill: false,
					label: col,
					borderColor: colors[ col ],
					data: [],
				};
			}
			
			const p = { x: moment( t ), y: data[r][i] };
			
			series[col].data.push( p );
		}
	};

	return {
		labels,
		datasets: Object.values( series )
	};
}
