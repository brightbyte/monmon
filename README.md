The **PET Monster Monitor** is a small utility for monitoring the progress of the [PET Team's](https://www.mediawiki.org/wiki/Core_Platform_Team) effort to reduce the usage of old "Monster" classes like User in Title in the MediaWiki code base.

It has two parts:
1) A web page that displays a graph, based on a CSV file.
2) A set of shell scripts to update the csv file, designed to run via cron.

# Scripts
`update-data <config> <target> <output> [timestamp]`: calculates the statistics defined by `<config>` about `<target>` and appends a line to `<output>`. If `[timestamp]` is given, the state of `<target>` at the given time is analyzed. `<target>` must be a directory in a git repo. ld in the output will be a timestamp - either the current day, or the day identified by `[timestamp]`. `[timestamp]` can be given in any form accepted by the GNU `date` utility, e.g. "2020-06-21" or "yesterday" or "3 weeks ago".

`count-monsters <config> <target> [timestamp]`: calculates the statistics defined by `<config>` about `<target>` and outputs them as a line of tab separated values. The first field in the output will be a timestamp - either the current day, or the day identified by `[timestamp]`. `[timestamp]` can be given in any form accepted by the GNU `date` utility, e.g. "2020-06-21" or "yesterday" or "3 weeks ago".

`find-commit <date> [dir] [branch]`: returns the hash of the last commit before `<date>`. `<date>` can be given in any form accepted by the GNU `date` utility, e.g. "2020-06-21" or "yesterday" or "3 weeks ago". `[dir]` is a directory in the git repo to target. `[branch]` is the branch to look on ("master" per default).

`regenerate-data <config> <target> <input> <output>`: generates the statistics defined by `<config>` into `<output>` for each timestamp present in `<input>`. This can be used to regenerate data after changing the configuration, or to generate data for a new configuration for the same points in time that have data for an old configuration.

# Libraries
* **Chart JS**: https://www.chartjs.org/ (MIT license)
* **OKFN CSV**: https://github.com/okfn/csv.js/ (MIT license)
* **Moment JS**: https://momentjs.com/ (MIT license)

# License
Written by Daniel Kinzler for the Wikimedia Foundation in 2021, published under the terms of the MIT license: 

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
